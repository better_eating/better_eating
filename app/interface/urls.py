from django.urls import path

from interface.views.consumption import ConsumptionHistoryView, ConsumptionAddView, ConsumptionUpdateView, \
    ConsumptionDeleteView
from interface.views.export import ExportSearchView
from interface.views.home import HomeView
from interface.views.ingredient import IngredientDeleteView, IngredientUpdateView, IngredientListView, \
    IngredientCreateView, IngredientMergeView
from interface.views.meal import MealListView, MealUpdateView, MealDeleteView, MealCreateView
from interface.views.mood import MoodHistoryView, MoodAddView, MoodUpdateView, MoodDeleteView
from interface.views.symptom import SymptomDeleteView, SymptomUpdateView, SymptomListView, SymptomCreateView

urlpatterns = [
    path('meals/delete/<int:id>', MealDeleteView.as_view(), name="meal_delete"),
    path('meals/<int:id>', MealUpdateView.as_view(), name="meal_update"),
    path('meals/', MealListView.as_view(), name="meals"),
    path('meals/create', MealCreateView.as_view(), name="meal_create"),

    path('ingredients/delete/<int:id>', IngredientDeleteView.as_view(), name="ingredient_delete"),
    path('ingredients/<int:id>', IngredientUpdateView.as_view(), name="ingredient_update"),
    path('ingredients/', IngredientListView.as_view(), name="ingredients"),
    path('ingredients/create', IngredientCreateView.as_view(), name="ingredient_create"),

    path('ingredients/merge/<int:source_ingredient_id>', IngredientMergeView.as_view(), name="ingredient_merge"),
    path('ingredients/merge/<int:source_ingredient_id>/<int:target_ingredient_id>', IngredientMergeView.as_view(), name="ingredient_merge"),

    path("mood/history", MoodHistoryView.as_view(), name="mood_history"),
    path("mood/add", MoodAddView.as_view(), name="mood_add"),
    path("mood/<int:id>", MoodUpdateView.as_view(), name="mood_update"),
    path("mood/<int:id>/delete", MoodDeleteView.as_view(), name="mood_delete"),

    path('symptoms/delete/<int:id>', SymptomDeleteView.as_view(), name="symptom_delete"),
    path('symptoms/<int:id>', SymptomUpdateView.as_view(), name="symptom_update"),
    path('symptoms/', SymptomListView.as_view(), name="symptoms"),
    path('symptoms/create', SymptomCreateView.as_view(), name="symptom_create"),

    path("consumption/history", ConsumptionHistoryView.as_view(), name="consumption_history"),
    path("consumption/add", ConsumptionAddView.as_view(), name="consumption_add"),
    path("consumption/<int:id>", ConsumptionUpdateView.as_view(), name="consumption_update"),
    path("consumption/<int:id>/delete", ConsumptionDeleteView.as_view(), name="consumption_delete"),

    path("export", ExportSearchView.as_view(), name="export"),

    path('', HomeView.as_view(), name="home"),
]
