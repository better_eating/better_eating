from django import forms
from django.utils.translation import gettext_lazy
from better_eating.custom_form_fields import DateInput


class ExportFilterForm(forms.Form):
    start_date = forms.DateField(
        widget=DateInput(),
        label=gettext_lazy("Start"),
        required=False,
    )
    end_date = forms.DateField(
        label=gettext_lazy("End"),
        widget=DateInput(),
        required=False,
    )