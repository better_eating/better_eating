import re

from django import forms
from django.utils.translation import gettext_lazy

from core.models.mood import Mood
from core.models.symptom import Symptom
from user.models import User


class MoodForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput(), required=False)

    datetime = forms.SplitDateTimeField(
        widget=forms.SplitDateTimeWidget(
            date_format="%Y-%m-%d",
            time_format="%H:%M",
            date_attrs={'type': 'date'},
            time_attrs={'type': 'time'},
        ),
        required=True
    )
    additional_symptoms = forms.CharField(
        label=gettext_lazy("Symptoms"),
        widget=forms.Textarea,
        required=False,
        help_text=gettext_lazy("Separate Symptoms by ';' or newline. Add an inensity(integer) like 'Stomachache 2'")
    )


    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user

        if commit:
            instance.save()
            self.save_m2m()

            if "additional_symptoms" in self.cleaned_data:
                additional_symptoms = self.cleaned_data.get("additional_symptoms")
                additional_symptoms_list = additional_symptoms.replace('\n', ';').split(";")
                for entry in additional_symptoms_list:
                    if entry:
                        intensity = None
                        intensity_string = None
                        matching = re.match('.*?([0-9]+)$', entry)
                        if matching:
                            intensity_string = matching.group(1)

                        if intensity_string:
                            if intensity_string in entry:
                                entry = entry.replace(intensity_string, "")
                            intensity = int(intensity_string)

                        name = entry.strip()

                        if name:
                            symptom = Symptom.objects.filter(
                                user=self.user,
                                name=name,
                                intensity=intensity,
                            ).first()
                            if not symptom:
                                symptom = Symptom.objects.create(
                                    user=self.user,
                                    name=name,
                                    intensity=intensity
                                )
                            instance.symptoms.add(symptom)
                instance.save()


        return instance

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)


    class Meta:
        model = Mood
        exclude = ["symptoms"]





class MoodFormWithSymptoms(forms.ModelForm):
    field_order = ['name', 'additional_ingredients', 'ingredients']

    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput(), required=False)

    datetime = forms.SplitDateTimeField(
        widget=forms.SplitDateTimeWidget(
            date_format="%Y-%m-%d",
            time_format="%H:%M",
            date_attrs={'type': 'date'},
            time_attrs={'type': 'time'},
        ),
        required=True
    )
    additional_symptoms = forms.CharField(
        label=gettext_lazy("Symptoms"),
        widget=forms.Textarea,
        required=False,
        help_text=gettext_lazy("Separate Symptoms by ';' or newline. Add an inensity(integer) like 'Stomachache 2'")
    )

    symptoms = forms.ModelMultipleChoiceField(
        queryset=Symptom.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
        label="Selected Symptoms"
    )
    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user

        if commit:
            instance.save()
            self.save_m2m()

            if "additional_symptoms" in self.cleaned_data:
                additional_symptoms = self.cleaned_data.get("additional_symptoms")
                additional_symptoms_list = additional_symptoms.replace('\n', ';').split(";")
                for entry in additional_symptoms_list:
                    if entry:
                        intensity = None

                        matched = re.match('.*?([0-9]+)$', entry)
                        if matched:
                            intensity_string = matched.group(1)
                            if intensity_string:
                                if intensity_string in entry:
                                    entry = entry.replace(intensity_string, "")
                                intensity = int(intensity_string)

                        name = entry.strip()

                        if name:
                            symptom = Symptom.objects.filter(
                                user=self.user,
                                name=name,
                                intensity=intensity,
                            ).first()
                            if not symptom:
                                symptom = Symptom.objects.create(
                                    user=self.user,
                                    name=name,
                                    intensity=intensity
                                )
                            instance.symptoms.add(symptom)
                instance.save()


        return instance

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        if self.user:
            #overwrite queryset
            available_symptoms = Symptom.objects.filter(
                user=self.user
            ).order_by("name")
            self.fields['symptoms'].queryset = available_symptoms

    class Meta:
        model = Mood
        fields = "__all__"