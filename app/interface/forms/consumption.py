from django import forms

from core.models.consumption import Consumption
from core.models.meal import Meal
from user.models import User


class ConsumptionForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput(), required=False)

    datetime = forms.SplitDateTimeField(
        widget=forms.SplitDateTimeWidget(
            date_format="%Y-%m-%d",
            time_format="%H:%M",
            date_attrs={'type': 'date'},
            time_attrs={'type': 'time'},
        ),
        required=True
    )
    meal = forms.ModelChoiceField(queryset=Meal.objects.none())

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user

        if commit:
            instance.save()
            self.save_m2m()

        return instance

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        if self.user:
            # overwrite queryset
            available_meals = Meal.objects.filter(
                user=self.user
            )
            print(available_meals)
            self.fields['meal'].queryset = available_meals


    class Meta:
        model = Consumption
        fields = "__all__"