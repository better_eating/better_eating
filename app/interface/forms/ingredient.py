from django import forms
from django.utils.translation import gettext_lazy

from core.models.ingredient import Ingredient
from user.models import User


class IngredientSearchForm(forms.Form):
    search = forms.CharField(
        label=gettext_lazy("Search"),
        widget=forms.TextInput(attrs={
            'autocomplete': 'off',
            "autocapitalize": "off",
            "spellcheck": "false",
            "autocorrect": "off",
        }),
        required=False
    )



class IngredientForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput(), required=False)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user

        if commit:
            instance.save()
            self.save_m2m()

        return instance

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)


    class Meta:
        model = Ingredient
        fields = "__all__"
