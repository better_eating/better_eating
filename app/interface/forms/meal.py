from django import forms
from django.utils.translation import gettext_lazy

from core.models.ingredient import Ingredient
from core.models.meal import Meal
from user.models import User


class MealSearchForm(forms.Form):
    search = forms.CharField(
        label=gettext_lazy("Search"),
        widget=forms.TextInput(attrs={
            'autocomplete': 'off',
            "autocapitalize": "off",
            "spellcheck": "false",
            "autocorrect": "off",
        }),
        required=False
    )



class MealForm(forms.ModelForm):
    #ingredients = forms.ModelMultipleChoiceField(
    #    queryset=Ingredient.objects.none(),
    #    widget=forms.CheckboxSelectMultiple,
    #    required=False
    #)
    additional_ingredients = forms.CharField(label=gettext_lazy('Ingredients'), widget=forms.Textarea, required=False, help_text=gettext_lazy("Separate Ingredients by ';'"))

    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput(), required=False)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user

        if commit:
            instance.save()
            self.save_m2m()

            if "additional_ingredients" in self.cleaned_data:
                additional_ingredients = self.cleaned_data.get("additional_ingredients")
                additional_ingredients_list = additional_ingredients.replace('\n', ';').split(";")
                for name in additional_ingredients_list:
                    if name:
                        ingredient = Ingredient.objects.filter(
                            user=self.user,
                            name=name.strip(),
                        ).first()
                        if not ingredient:
                            ingredient = Ingredient.objects.create(
                                user=self.user,
                                name=name.strip(),
                            )
                        instance.ingredients.add(ingredient)
                instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        #if self.user:

            # overwrite queryset
            #available_ingredients = Ingredient.objects.filter(
            #    user=self.user
            #)
            #self.fields['ingredients'].queryset = available_ingredients



    class Meta:
        model = Meal
        exclude = ["ingredients"]

class MealFormWithIngredients(forms.ModelForm):
    field_order = ['name', 'additional_ingredients', 'ingredients']

    additional_ingredients = forms.CharField(label="Additional Ingredients", widget=forms.Textarea, required=False, help_text=gettext_lazy("Separate Ingredients by ';'"))
    ingredients = forms.ModelMultipleChoiceField(
        queryset=Ingredient.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
        label="Selected Ingredients"
    )
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput(), required=False)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.user = self.user

        if commit:
            instance.save()
            self.save_m2m()

            if "additional_ingredients" in self.cleaned_data:
                additional_ingredients = self.cleaned_data.get("additional_ingredients")
                additional_ingredients_list = additional_ingredients.replace('\n', ';').split(";")
                for name in additional_ingredients_list:
                    if name:
                        ingredient, created = Ingredient.objects.get_or_create(
                            user=self.user,
                            name=name.strip(),
                        )
                        instance.ingredients.add(ingredient)
                instance.save()

        return instance

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        if self.user:
            #overwrite queryset
            available_ingredients = Ingredient.objects.filter(
                user=self.user
            )
            self.fields['ingredients'].queryset = available_ingredients


    class Meta:
        model = Meal
        fields  = "__all__"
