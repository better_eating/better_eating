from io import BytesIO

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, StreamingHttpResponse
from django.template import loader
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.text import slugify
#from django.utils.translation import gettext_lazy as _
from django.utils.translation import gettext as _
from django.views import View
import xlsxwriter

from interface.forms.export import ExportFilterForm
from interface.helper.export import get_export_data_for_user


@method_decorator(login_required, name='dispatch')
class ExportSearchView(View):
    def get(self, request):
        template = loader.get_template("interface/export.html")
        context = {}

        form = ExportFilterForm()
        context["form"] = form

        data = get_export_data_for_user(user=request.user)

        context["data"] = data
        return HttpResponse(template.render(context, request))


    def post(self, request):
        template = loader.get_template("interface/export.html")
        context = {}

        form = ExportFilterForm(request.POST)


        if form.is_valid():
            data = get_export_data_for_user(
                user=request.user,
                start_date=form.cleaned_data.get("start_date"),
                end_date=form.cleaned_data.get("end_date"),
            )
        else:
            data = get_export_data_for_user(user=request.user)


        context["form"] = form
        context["data"] = data

        if request.POST['action'] == "export":
            output = BytesIO()
            book = xlsxwriter.Workbook(output)
            sheet = book.add_worksheet(request.user.get_full_name())

            date_time_format = book.add_format({'num_format': 'dd.mm.yyyy HH:MM'})
            date_format = book.add_format({'num_format': 'dd.mm.yyyy'})
            time_format = book.add_format({'num_format': 'HH:MM'})

            wrap_format = book.add_format()
            wrap_format.set_text_wrap()

            title_format = book.add_format()
            title_format.set_bold()
            title_format.set_font_size(16)
            title_format.set_bottom(2)

            format_bad = book.add_format({
                'bg_color': '#FFC7CE',
                'font_color': '#9C0006'
            })

            format_good = book.add_format({
                'bg_color': '#C6EFCE',
                'font_color': '#006100'}
            )

            sheet.set_column(0, 1, 12)
            sheet.set_column(2, 2, 10)
            sheet.set_column(3, 3, 30)
            sheet.set_column(4, 4, 40)
            sheet.set_column(5, 7, 20)
            sheet.set_column(8, 8, 30)


            titles = [
                _("Date"),
                _("Time"),
                _("Amount"),
                _("Meal"),
                _("Ingredients"),
                _("Emotional State"),
                _("Energy Level"),
                _("Digestion"),
                _("Symptoms")
            ]
            for column, title in enumerate(titles):
                sheet.write(0, column, title, title_format)


            sheet.set_row(0, 20)
            row = 1

            for element in data:

                sheet.write_datetime(row, 0, timezone.make_naive(element.get("datetime")).date(), date_format)
                sheet.write_datetime(row, 1, timezone.make_naive(element.get("datetime")).time(), time_format)

                if element.get("type") == "consumption":
                    consumption = element.get("instance")
                    sheet.write(row, 2, consumption.amount)
                    sheet.write(row, 3, consumption.meal.name)
                    sheet.write(
                        row,
                        4,
                        ", ".join([ingredient.name for ingredient in consumption.meal.ingredients.all()]),
                        wrap_format
                    )

                if element.get("type") == "mood":
                    mood = element.get("instance")
                    sheet.write(row, 5, mood.emotional_state)
                    sheet.write(row, 6, mood.energy_level)
                    sheet.write(row, 7, mood.digestion)
                    sheet.write(
                        row,
                        8,
                        ", ".join([symptom.__str__() for symptom in mood.symptoms.all()]),
                        wrap_format
                    )

                row = row + 1

            sheet.conditional_format(
                f'F2:H{row + 1}',
                {
                    'type': 'cell',
                    'criteria': '>',
                    'value': 3,
                    'format': format_good
                }
            )
            sheet.conditional_format(
                f'F2:H{row + 1}',
                {
                    'type': 'cell',
                    'criteria': 'between',
                    'minimum': 1,
                    'maximum': 3,
                    'format': format_bad
                }
            )

            book.close()
            output.seek(0)  # seek stream on begin to retrieve all data from it
            # send "output" object to stream with mimetype and filename
            response = StreamingHttpResponse(
                output, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            )
            response['Content-Disposition'] = 'attachment; filename=BetterEatingExport_'+ slugify(request.user.get_full_name()) + '.xlsx'
            return response

        return HttpResponse(template.render(context, request))

