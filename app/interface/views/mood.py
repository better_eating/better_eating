from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.template import loader
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View

from core.models.mood import Mood
from interface.forms.mood import MoodForm, MoodFormWithSymptoms


@method_decorator(login_required, name='dispatch')
class MoodHistoryView(View):
    def get(self, request):
        template = loader.get_template("interface/mood_history.html")
        context = {}
        moods = Mood.objects.filter(
            user=request.user,
        )

        paginator = Paginator(moods, 20)

        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context["page_obj"] = page_obj

        return HttpResponse(template.render(context, request))



@method_decorator(login_required, name='dispatch')
class MoodAddView(View):
    def get(self, request):
        template = loader.get_template("interface/mood_add.html")
        context = {}
        form = MoodForm(
            user=request.user,
            initial={
                "datetime": timezone.now()
            },
        )
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request):
        template = loader.get_template("interface/mood_add.html")
        context = {}
        form = MoodForm(request.POST, user=request.user)
        if form.is_valid():
            context["form"] = form
            form.save()
            return redirect("mood_history")
        else:
            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class MoodUpdateView(View):
    def get(self, request, id):
        template = loader.get_template("interface/mood_update.html")
        context = {}

        mood = get_object_or_404(Mood, id=id, user=request.user)
        context["mood"] = mood

        form = MoodFormWithSymptoms(instance=mood, user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request, id):
        template = loader.get_template("interface/mood_update.html")
        context = {}

        mood = get_object_or_404(Mood, id=id)
        context["mood"] = mood

        form = MoodFormWithSymptoms(request.POST, instance=mood, user=request.user)

        if form.is_valid():
            form.save()
            return redirect("mood_history")
        else:
            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class MoodDeleteView(View):
    def post(self,request, id):
        mood = get_object_or_404(Mood, id=id, user=request.user)
        mood.delete()
        return redirect("mood_history")
