from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View

from core.helper.ingredients import get_mean_from_ingredient_for_user
from core.models.consumption import Consumption
from core.models.ingredient import Ingredient
from core.models.meal import Meal
from core.models.mood import Mood


def get_ingredient_statistics_data(user, search=None):
    data = []
    ingredients = Ingredient.objects.filter(
        user=user,
    )

    if search:
        ingredients = ingredients.filter(
            name__icontains=search
        )

    for ingredient in ingredients:
        data.append({
            "ingredient": ingredient,
            "mean_mood": get_mean_from_ingredient_for_user(user, ingredient),
        })

    return data
