from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View

from core.models.symptom import Symptom
from interface.forms.symptom import SymptomForm, SymptomSearchForm


@method_decorator(login_required, name='dispatch')
class SymptomCreateView(View):
    def get(self, request):
        template = loader.get_template("interface/symptom_create.html")

        context = {}
        form = SymptomForm(user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request):
            template = loader.get_template("interface/symptom_create.html")
            context = {}

            form = SymptomForm(request.POST, user=request.user)

            if form.is_valid():
                form.save()
                return redirect("symptoms")

            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class SymptomListView(View):
    def get(self, request):
        # list all
        template = loader.get_template("interface/symptom_list.html")
        context = {}


        form = SymptomSearchForm()
        context["form"] = form

        symptoms = Symptom.objects.filter(
            user=request.user,
        )

        paginator = Paginator(symptoms, 20)

        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context["page_obj"] = page_obj


        return HttpResponse(template.render(context, request))

    def post(self, request):
        template = loader.get_template("interface/symptom_list.html")
        context = {}

        form = SymptomSearchForm(request.POST)
        context["form"] = form

        search = None
        if form.is_valid():
            search = form.cleaned_data.get("search")
        symptoms = Symptom.objects.filter(
            user=request.user,
        )
        if search:
            symptoms = symptoms.filter(
                name__icontains=search
            )

        paginator = Paginator(symptoms, 20)

        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context["page_obj"] = page_obj



        return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class SymptomUpdateView(View):
    def get(self, request, id):
        # list for update
        template = loader.get_template("interface/symptom_edit.html")
        context = {}

        symptom = get_object_or_404(Symptom, id=id)
        context["symptom"] = symptom
        form = SymptomForm(instance=symptom, user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request, id):
        template = loader.get_template("interface/symptom_edit.html")
        context = {}

        symptom = get_object_or_404(Symptom, id=id)
        context["symptom"] = symptom
        form = SymptomForm(request.POST, instance=symptom, user=request.user)
        context["form"] = form

        if form.is_valid():
            form.save()
            return redirect("symptoms")

        return HttpResponse(template.render(context, request))

class SymptomDeleteView(View):
    def post(self, request, id):

        symptom = get_object_or_404(Symptom, id=id, user=request.user)

        symptom.delete()
        return redirect("symptoms")

