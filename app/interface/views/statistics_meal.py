from core.helper.meals import get_mean_from_meal_for_user
from core.models.consumption import Consumption
from core.models.meal import Meal
from core.models.mood import Mood



def get_meal_statistics_data(user, search=None):
    data = []

    meals = Meal.objects.filter(
        user=user,
    )

    if search:
        meals = meals.filter(
            name__icontains=search
        )

    for meal in meals:
        mean_mood = 0
        counter = 0

        consumptions = Consumption.objects.filter(
            user=user,
            meal=meal
        ).order_by("datetime")

        for consumption in consumptions:
            mood = Mood.objects.filter(
                user=user,
                datetime__gte=consumption.datetime,
            ).order_by(
                "datetime"
            ).first()
            if mood:
                print(mood)
                mean_mood += mood.mean()
                print(mean_mood)
                counter += 11

        if counter:
            mean_mood = mean_mood / counter

        if mean_mood == 0:
            mean_mood = None

        data.append({
            "meal": meal,
            "mean_mood": get_mean_from_meal_for_user(user, meal),
        })

    return data
