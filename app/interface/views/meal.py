
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views import View

from core.models.meal import Meal
from interface.forms.meal import MealForm, MealSearchForm, MealFormWithIngredients
from interface.views.statistics_meal import get_meal_statistics_data


@method_decorator(login_required, name='dispatch')
class MealCreateView(View):
    def get(self, request):
        template = loader.get_template("interface/meal_create.html")

        context = {}
        form = MealForm(user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request):
            template = loader.get_template("interface/meal_create.html")
            context = {}

            form = MealForm(request.POST, user=request.user)

            if form.is_valid():
                form.save()
                return redirect("meals")

            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class MealListView(View):
    def get(self, request):
        # list all
        template = loader.get_template("interface/meal_list.html")
        context = {}

        meals = Meal.objects.filter(
            user=request.user,
        )
        meal_data = get_meal_statistics_data(request.user)
        context["meal_data"] = meal_data

        form = MealSearchForm()
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request):
        template = loader.get_template("interface/meal_list.html")
        context = {}

        form = MealSearchForm(request.POST)
        context["form"] = form
        search = None
        if form.is_valid():
            search = form.cleaned_data.get("search")
        meal_data = get_meal_statistics_data(
            user=request.user,
            search=search
        )
        context["meal_data"] = meal_data

        return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class MealUpdateView(View):
    def get(self, request, id):
        # list for update
        template = loader.get_template("interface/meal_edit.html")
        context = {}

        meal = get_object_or_404(Meal, id=id)
        context["meal"] = meal
        form = MealFormWithIngredients(instance=meal, user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request, id):
        template = loader.get_template("interface/meal_edit.html")
        context = {}

        meal = get_object_or_404(Meal, id=id)
        context["meal"] = meal
        form = MealFormWithIngredients(request.POST, instance=meal, user=request.user)
        context["form"] = form

        if form.is_valid():
            form.save()
            return redirect("meals")

        return HttpResponse(template.render(context, request))

class MealDeleteView(View):
    def post(self, request, id):

        meal = get_object_or_404(Meal, id=id, user=request.user)

        meal.delete()
        return redirect("meals")
