from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View

from core.models.consumption import Consumption
from core.models.mood import Mood

@method_decorator(login_required, name='dispatch')
class HomeView(View):
    def get(self, request):
        template = loader.get_template("interface/home.html")
        context = {}

        latest_consumption = Consumption.objects.filter(
            user=request.user,
        ).order_by("-datetime").first()
        context["latest_consumption"] = latest_consumption
        consumption_too_old = False
        if latest_consumption and (timezone.now() - latest_consumption.datetime).days > 0:
            consumption_too_old = True
        context["consumption_too_old"] = consumption_too_old

        latest_mood = Mood.objects.filter(
            user=request.user,
        ).order_by("-datetime").first()
        context["latest_mood"] = latest_mood
        mood_too_old = False
        if latest_mood and (timezone.now() - latest_mood.datetime).days > 0:
            mood_too_old = True
        context["mood_too_old"] = mood_too_old
        return HttpResponse(template.render(context, request))
