from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.template import loader
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View

from core.models.ingredient import Ingredient
from core.models.meal import Meal
from interface.forms.ingredient import IngredientForm, IngredientSearchForm
from interface.views.statistics_ingredient import get_ingredient_statistics_data


@method_decorator(login_required, name='dispatch')
class IngredientCreateView(View):
    def get(self, request):
        template = loader.get_template("interface/ingredient_create.html")

        context = {}
        form = IngredientForm(user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request):
            template = loader.get_template("interface/ingredient_create.html")
            context = {}

            form = IngredientForm(request.POST, user=request.user)

            if form.is_valid():
                form.save()
                return redirect("ingredients")

            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class IngredientListView(View):
    def get(self, request):
        # list all
        template = loader.get_template("interface/ingredient_list.html")
        context = {}

        ingredient_data = get_ingredient_statistics_data(user=request.user)
        context["ingredient_data"] = ingredient_data

        form = IngredientSearchForm()
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request):
        template = loader.get_template("interface/ingredient_list.html")
        context = {}

        form = IngredientSearchForm(request.POST)
        context["form"] = form

        search = None
        if form.is_valid():
            search = form.cleaned_data.get("search")

        ingredient_data = get_ingredient_statistics_data(
            user=request.user,
            search=search
        )
        context["ingredient_data"] = ingredient_data

        return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class IngredientUpdateView(View):
    def get(self, request, id):
        # list for update
        template = loader.get_template("interface/ingredient_edit.html")
        context = {}

        ingredient = get_object_or_404(Ingredient, id=id)
        context["ingredient"] = ingredient
        form = IngredientForm(instance=ingredient, user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request, id):
        template = loader.get_template("interface/ingredient_edit.html")
        context = {}

        ingredient = get_object_or_404(Ingredient, id=id)
        context["ingredient"] = ingredient
        form = IngredientForm(request.POST, instance=ingredient, user=request.user)
        context["form"] = form

        if form.is_valid():
            form.save()
            return redirect("ingredients")

        return HttpResponse(template.render(context, request))

class IngredientDeleteView(View):
    def post(self, request, id):

        ingredient = get_object_or_404(Ingredient, id=id, user=request.user)

        ingredient.delete()
        return redirect("ingredients")


class IngredientMergeView(View):
    '''
    merge source ingredient into target ingredient
    - replace source with target in meals
    - delete source
    - optional: give new name
    '''
    def get(self, request, source_ingredient_id, target_ingredient_id=None):
        template = loader.get_template("interface/ingredient_merge.html")
        context = {}
        source_ingredient = get_object_or_404(Ingredient, id=source_ingredient_id)
        context["source_ingredient"] = source_ingredient

        if target_ingredient_id:
            target_ingredient = get_object_or_404(Ingredient, id=target_ingredient_id)
            context["target_ingredient"] = target_ingredient

        ingredients = Ingredient.objects.filter(
            user=request.user
        )
        ingredients = ingredients.exclude(
            id=source_ingredient_id
        )
        context["ingredients"] = ingredients

        return HttpResponse(template.render(context, request))


    def post(self, request, source_ingredient_id, target_ingredient_id):
        source_ingredient = get_object_or_404(Ingredient, id=source_ingredient_id)
        target_ingredient = get_object_or_404(Ingredient, id=target_ingredient_id)
        meals = Meal.objects.filter(
            ingredients__in=[source_ingredient]
        )
        for meal in meals:
            meal.ingredients.remove(source_ingredient)
            meal.ingredients.add(target_ingredient)

        source_ingredient.delete()

        return redirect("ingredients")
