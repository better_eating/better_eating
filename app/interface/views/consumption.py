from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.template import loader
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views import View

from core.models.consumption import Consumption
from core.models.meal import Meal
from interface.forms.consumption import ConsumptionForm


@method_decorator(login_required, name='dispatch')
class ConsumptionHistoryView(View):
    def get(self, request):
        template = loader.get_template("interface/consumption_history.html")
        context = {}
        consumptions = Consumption.objects.filter(
            user=request.user,
        )

        paginator = Paginator(consumptions, 20)

        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context["page_obj"] = page_obj

        return HttpResponse(template.render(context, request))



@method_decorator(login_required, name='dispatch')
class ConsumptionAddView(View):
    def get(self, request):
        template = loader.get_template("interface/consuption_add.html")
        context = {}

        form = ConsumptionForm(
            user=request.user,
            initial={
                "datetime": timezone.now()
            },
        )

        meal_id = request.GET.get('meal', None)
        print(meal_id)
        if meal_id:
            meal = Meal.objects.filter(
                id=meal_id,
                user=request.user,
            ).first()
            print(meal)
            if meal:
                form = ConsumptionForm(
                    user=request.user,
                    initial={
                        "meal": meal,
                        "datetime": timezone.now()
                    },
                )
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request):
        template = loader.get_template("interface/consuption_add.html")
        context = {}
        form = ConsumptionForm(request.POST, user=request.user)
        if form.is_valid():
            context["form"] = form
            consumption = form.save()
            messages.add_message(
                request,
                messages.SUCCESS,
                f'Logged: {consumption.meal.name} @ {consumption.datetime.strftime("%Y-%m-%d %H:%M")}'
            )
            return redirect("meals")
        else:
            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class ConsumptionUpdateView(View):
    def get(self, request, id):
        template = loader.get_template("interface/consuption_update.html")
        context = {}

        consumption = get_object_or_404(Consumption, id=id, user=request.user)
        context["consumption"] = consumption

        form = ConsumptionForm(instance=consumption, user=request.user)
        context["form"] = form

        return HttpResponse(template.render(context, request))

    def post(self, request, id):
        template = loader.get_template("interface/consuption_update.html")
        context = {}

        consumption = get_object_or_404(Consumption, id=id)
        context["consumption"] = consumption

        form = ConsumptionForm(request.POST, instance=consumption, user=request.user)

        if form.is_valid():
            form.save()
            return redirect("consumption_history")
        else:
            context["form"] = form
            return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class ConsumptionDeleteView(View):
    def post(self,request, id):
        consumption = get_object_or_404(Consumption, id=id, user=request.user)
        consumption.delete()
        return redirect("consumption_history")
