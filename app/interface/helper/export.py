from core.models.consumption import Consumption
from core.models.mood import Mood


def get_export_data_for_user(user, start_date=None, end_date=None):

    data = []
    consumptions = Consumption.objects.filter(
        user=user
    ).all()

    moods = Mood.objects.filter(
        user=user
    ).all()

    if start_date:
        consumptions = consumptions.filter(
            datetime__date__gte=start_date,
        )
        moods = moods.filter(
            datetime__date__gte=start_date,
        )

    if end_date:
        consumptions = consumptions.filter(
            datetime__date__lte=end_date,
        )
        moods = moods.filter(
            datetime__date__lte=end_date,
        )

    for consumption in consumptions:
        data.append({
            "datetime": consumption.datetime,
            "type": "consumption",
            "instance": consumption
        })
    for mood in moods:
        data.append({
            "datetime": mood.datetime,
            "type": "mood",
            "instance": mood
        })

    data = sorted(data, key=lambda x: x["datetime"], reverse=False)

    return data