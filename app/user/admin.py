from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.
from user.models import User

# Register your models here.
admin.site.register(User, UserAdmin)

# extend fields with custom ones
#UserAdmin.fieldsets += ()

