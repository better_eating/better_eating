from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    def get_full_name(self):
        full_name = self.username
        if self.first_name and self.last_name:
            full_name = "{} {}".format(self.first_name, self.last_name)
        return full_name
