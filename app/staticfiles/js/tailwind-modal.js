modal_open = false;
current_modal_id = null;

function toggleModal(modalId){
    modal = document.getElementById(modalId);
    current_modal_id = modalId;
    if (modal_open){
        closeModal(modalId);
    }else{
        openModal(modalId);
    }
}


function closeModal(modalId){
    modal.classList.add("hidden")
    modal_open = false;
    current_modal_id = null;
}

function openModal(modalId){
    modal.classList.remove("hidden")
    modal_open = true;
}


// close modal on escape
document.body.onkeydown = function (event){
    if (event.key === "Escape"){
        if(modal_open && current_modal_id){
            closeModal(current_modal_id);
        }
    }
}
