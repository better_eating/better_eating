// This is a minimal config.
// If you need the full config, get it from here:
// https://raw.githubusercontent.com/tailwindlabs/tailwindcss/v1/stubs/defaultConfig.stub.js

const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  future: {},
  content: [
    // Templates within theme app (e.g. base.html)
    './templates/**/*.html',
    // Templates in other apps
    './**/templates/**/*.html',
  ],
  plugins: [],
}