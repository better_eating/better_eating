from core.models.consumption import Consumption
from core.models.mood import Mood


def get_mean_from_meal_for_user(user, meal):
    '''
    returns the mean mood over all data for a meal for a user
    '''
    mean_mood = 0
    counter = 0

    consumptions = Consumption.objects.filter(
        user=user,
        meal=meal
    ).order_by("datetime")

    for consumption in consumptions:
        mood = Mood.objects.filter(
            user=user,
            datetime__gte=consumption.datetime,
        ).order_by(
            "datetime"
        ).first()
        if mood:
            print(mood)
            mean_mood += mood.mean()
            print(mean_mood)
            counter += 1

    if counter:
        mean_mood = mean_mood / counter

    if mean_mood == 0:
        mean_mood = None

    return mean_mood
