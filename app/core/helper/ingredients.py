from core.models.consumption import Consumption
from core.models.meal import Meal
from core.models.mood import Mood


def get_mean_from_ingredient_for_user(user, ingredient):
    '''
    calculates the mean mood value for an ingredient
    across all entries in the database
    '''
    meals = Meal.objects.filter(
        user=user,
        ingredients__in=[ingredient]
    )
    mean_mood = 0
    counter = 0

    for meal in meals:

        consumptions = Consumption.objects.filter(
            user=user,
            meal=meal
        ).order_by("datetime")

        for consumption in consumptions:
            mood = Mood.objects.filter(
                user=user,
                datetime__gte=consumption.datetime,
            ).order_by(
                "datetime"
            ).first()
            if mood:
                mean_mood += mood.mean()
                counter += 1

    if counter:
        mean_mood = mean_mood / counter

    if mean_mood == 0:
        mean_mood = None

    return mean_mood
