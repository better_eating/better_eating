from django.contrib import admin

# Register your models here.
from core.models.consumption import Consumption
from core.models.ingredient import Ingredient
from core.models.meal import Meal
from core.models.mood import Mood
from core.models.symptom import Symptom


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = ["name", "user"]


@admin.register(Meal)
class MealAdmin(admin.ModelAdmin):
    list_display = ["name", "user"]
    list_filter = ["user"]
    filter_horizontal = ["ingredients"]

@admin.register(Mood)
class MoodAdmin(admin.ModelAdmin):
    list_display = ["datetime", "emotional_state", "energy_level", "digestion"]
    list_filter = ["user"]
    filter_horizontal = ["symptoms"]

@admin.register(Symptom)
class SymptomAdmin(admin.ModelAdmin):
    list_display = ["name", "user"]
    list_filter = ["user"]

@admin.register(Consumption)
class ConsumptionAdmin(admin.ModelAdmin):
    list_display = ["user", "meal", "datetime"]
    list_filter = ["user"]



