from django.db import models
from django.utils.translation import gettext_lazy

from user.models import User


class Ingredient(models.Model):
    name = models.CharField(verbose_name=gettext_lazy("Name"), max_length=999)
    user = models.ForeignKey(User, verbose_name=gettext_lazy("User"), on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name", )
        verbose_name = gettext_lazy("Ingredient")
        verbose_name_plural = gettext_lazy("Ingredients")
