from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy

from user.models import User

class Symptom(models.Model):
    name = models.CharField(verbose_name=gettext_lazy("Name"), max_length=999)
    intensity = models.PositiveIntegerField(
        verbose_name=gettext_lazy("Intensity"),
        null=True,
        blank=True,
    )

    user = models.ForeignKey(User, verbose_name=gettext_lazy("User"), on_delete=models.CASCADE)

    def __str__(self):
        if self.intensity:
            return f'{self.name}: {self.intensity}'
        else:
            return self.name

    class Meta:
        ordering = ("name", "intensity")
        unique_together = ["name", "user", "intensity"]
        verbose_name = gettext_lazy("Symptom")
        verbose_name_plural = gettext_lazy("Symptoms")
