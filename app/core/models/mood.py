from django.db import models
from django.template.defaultfilters import floatformat
from django.utils.translation import gettext_lazy

from core.models.symptom import Symptom
from user.models import User

CHOICES = [(i,i) for i in range(6)]

class Mood(models.Model):
    emotional_state = models.IntegerField(verbose_name=gettext_lazy("Emotional State"), choices=CHOICES)
    energy_level = models.IntegerField(verbose_name=gettext_lazy("Energy Level"), choices=CHOICES)
    digestion = models.IntegerField(verbose_name=gettext_lazy("Digestion"), choices=CHOICES)
    user = models.ForeignKey(User, verbose_name=gettext_lazy("User"), on_delete=models.CASCADE)
    datetime = models.DateTimeField(verbose_name=gettext_lazy("Datetime"))
    symptoms = models.ManyToManyField(Symptom, related_name="moods", blank=True, verbose_name=gettext_lazy("Symptoms"))

    def mean(self):
        return (self.emotional_state + self.energy_level + self.digestion) / 3

    def mean_formatted(self):
        return floatformat(self.mean(), 2)

    def __str__(self):
        return f'Mood {self.user.__str__()} at {self.datetime.strftime("%Y-%m-%d %H:%M")}: {self.mean()}'

    class Meta:
        ordering = ("-datetime",)
        verbose_name = gettext_lazy("Mood")
        verbose_name_plural = gettext_lazy("Moods")
