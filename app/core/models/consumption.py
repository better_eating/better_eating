from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy

from core.models.meal import Meal
from user.models import User


class Consumption(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=gettext_lazy("User"))
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE, verbose_name=gettext_lazy("Meal"))
    amount = models.DecimalField(
        verbose_name=gettext_lazy("Amount"),
        max_digits=2,
        decimal_places=1,
        null=True,
        blank=True,
        validators=[MinValueValidator(0)],
    )
    datetime = models.DateTimeField(verbose_name=gettext_lazy("Datetime"))


    def __str__(self):
        return f'{self.meal.name} @ {self.datetime.strftime("%Y-%m-%d")}'

    class Meta:
        ordering = ("-datetime",)
        verbose_name = gettext_lazy("Consumption")
        verbose_name_plural = gettext_lazy("Consumptions")
