from django.db import models
from django.utils.translation import gettext_lazy

from core.models.ingredient import Ingredient
from user.models import User


class Meal(models.Model):
    name = models.CharField(max_length=999, verbose_name=gettext_lazy("Name"))
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=gettext_lazy("User"))
    ingredients = models.ManyToManyField(Ingredient, related_name="meals", blank=True, verbose_name=gettext_lazy("Ingredients"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
        verbose_name = gettext_lazy("Meal")
        verbose_name_plural = gettext_lazy("Meals")
