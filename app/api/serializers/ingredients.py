from rest_framework import serializers

from core.helper.ingredients import get_mean_from_ingredient_for_user
from core.models.ingredient import Ingredient


class IngredientSerializer(serializers.ModelSerializer):
    mean_mood = serializers.SerializerMethodField()

    def get_mean_mood(self, instance):
        if self.context and "user" in self.context:
            return get_mean_from_ingredient_for_user(
                user=self.context.get("user"),
                ingredient=instance
            )

    class Meta:
        model = Ingredient
        exclude = ["user"]