from rest_framework import serializers

from core.helper.meals import get_mean_from_meal_for_user
from core.models.meal import Meal


class MealSerializer(serializers.ModelSerializer):
    mean_mood = serializers.SerializerMethodField()

    def get_mean_mood(self, instance):
        if self.context and "user" in self.context:
            return get_mean_from_meal_for_user(
                user=self.context.get("user"),
                meal=instance
            )

    class Meta:
        model = Meal
        exclude = ["user"]