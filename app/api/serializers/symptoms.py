from rest_framework import serializers

from core.models.symptom import Symptom


class SymptomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Symptom
        exclude = ["user"]