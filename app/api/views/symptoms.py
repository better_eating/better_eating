from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers.symptoms import SymptomSerializer
from core.models.symptom import Symptom


class SymptomView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        symptoms = Symptom.objects.filter(
            user=request.user
        )
        serializer = SymptomSerializer(
            symptoms,
            many=True
        )

        return Response(serializer.data)

    def put(self, request, symptom_id):
        symptom = get_object_or_404(Symptom, id=symptom_id)

        serializer = SymptomSerializer(data=request.data, instance=symptom)
        if serializer.is_valid():
            serializer.save()
            return Response()
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
