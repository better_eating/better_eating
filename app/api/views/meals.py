from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers.meals import MealSerializer
from core.models.meal import Meal


class MealView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        meals = Meal.objects.filter(
            user=request.user
        )
        serializer = MealSerializer(
            meals,
            many=True,
            context={'user': request.user}
        )

        return Response(serializer.data)
