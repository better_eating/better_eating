from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers.ingredients import IngredientSerializer
from core.models.ingredient import Ingredient


class IngredientView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        ingredients = Ingredient.objects.filter(
            user=request.user
        )
        serializer = IngredientSerializer(
            ingredients,
            many=True,
            context={'user': request.user}
        )

        return Response(serializer.data)
