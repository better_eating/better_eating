from django.urls import include, path

from api.views.ingredients import IngredientView
from api.views.meals import MealView
from api.views.symptoms import SymptomView
from rest_framework.authtoken import views

urlpatterns = [
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', views.obtain_auth_token),

    path('symptoms/', SymptomView.as_view()),
    path('symptoms/<int:symptom_id>', SymptomView.as_view()),

    path('ingredients/', IngredientView.as_view()),
    path('meals/', MealView.as_view()),
]